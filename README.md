# GOTENBERG

Récupérer le projet :
```shell script
$ git clone git@gitlab.com:edrichard/gotenberg.git
```

Construction de l'environnement :
```shell script
$ docker-compose up --build -d
```
Installation des `vendor` :
```shell script
$ docker-compose exec web composer install
```

Teste de l'environnement :
- Accès à `gotenberg` : http://localhost:3000/
- Accès à  `web` : http://localhost:8282/
