<?php
use TheCodingMachine\Gotenberg\Client;
use TheCodingMachine\Gotenberg\DocumentFactory;
use TheCodingMachine\Gotenberg\HTMLRequest;

require './vendor/autoload.php';

ob_start();
require 'pdf.php';
$content = ob_get_clean();

$client = new Client('gotemberg:3000', new \Http\Adapter\Guzzle6\Client());
$index = DocumentFactory::makeFromString('index.html', $content);
$footer = DocumentFactory::makeFromPath('footer.html', 'footer.html');
$assets = [
    DocumentFactory::makeFromPath('style.css', 'style.css'),
    DocumentFactory::makeFromPath('logo.png', 'logo.png'),
];
$request = new HTMLRequest($index);
$request->setAssets($assets);
$request->setFooter($footer);
$client->store($request, './pdf/facture.pdf');
